package com.example.tp3_web;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.io.File;

public class TeamAdapter extends SimpleCursorAdapter {
    private Cursor m_cursor;
    private Context m_context;
    private LayoutInflater miInflater;
    View convertView = null;

    TextView teamName,teamLigue,teamLastMatch;
    ImageView teamImage;


    public TeamAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        m_context = context;
        m_cursor = c;
    }

    @Override
    public void bindView(View arg0, Context arg1, Cursor arg2) {

        if (arg0 == null) {
            convertView = miInflater.inflate(R.layout.content_listview, null);
        } else {
            convertView = arg0;
        }

        teamImage = (ImageView) convertView.findViewById(R.id.teamImage);
        teamName = (TextView) convertView.findViewById(R.id.teamName);
        teamLigue = (TextView) convertView.findViewById(R.id.teamLigue);
        teamLastMatch = (TextView) convertView.findViewById(R.id.teamLastMatch);

        teamName.setText(arg2.getString(arg2.getColumnIndex(SportDbHelper.COLUMN_TEAM_NAME)));
        teamLigue.setText(arg2.getString(arg2.getColumnIndex(SportDbHelper.COLUMN_LEAGUE_NAME)));
        teamLastMatch.setText(arg2.getString(arg2.getColumnIndex(SportDbHelper.COLUMN_LAST_MATCH_LABEL)));

        Long teamId = m_cursor.getLong(m_cursor.getColumnIndex(SportDbHelper.COLUMN_TEAM_ID));
       /* if (teamId!=0) {
            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/hana/image/" + teamId + ".png");
            String filePath = file.getPath();
            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            teamImage.setImageBitmap(bitmap);
        }*/
    }

}

