package com.example.tp3_web;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class UpdateInfos {

    JSONResponseHandlerTeam jsonResponse = null;
    Team team;
    Bitmap bitmap = null;

    public UpdateInfos(JSONResponseHandlerTeam jsonResponseHandlerTeam, Team team){
        this.jsonResponse = jsonResponseHandlerTeam;
        this.team = team;
    }

    public void execute(){
        try {
            for (int i=0;i<3;i++){
                URL url = null;
                if (i==0){
                    url = WebServiceUrl.buildSearchTeam(team.getName());
                }else if (i==1){
                    url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                }else{
                    url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                }
                Log.i("url",url.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setConnectTimeout(7 * 1000);
                conn.connect();
                InputStream inputStream = conn.getInputStream();
                jsonResponse.readJsonStream(inputStream);
                conn.disconnect();
            }

            GetImageInputStream(team.getTeamBadge());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void GetImageInputStream(String imageUrl){
        URL url;
        HttpURLConnection connection=null;
        try {
            url = new URL(imageUrl);
            connection=(HttpURLConnection)url.openConnection();
            connection.setConnectTimeout(6000);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            InputStream inputStream=connection.getInputStream();
            bitmap= BitmapFactory.decodeStream(inputStream);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        SavaImage(Environment.getExternalStorageDirectory().getAbsolutePath()+"/hana/image/");
    }

    public void SavaImage(String path) {
        File file = new File(path + team.getIdTeam() + ".png");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 80, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
